import { useState } from "react";
import '../App.css';

export const StepForm3 = (props) => {
  const [data, setData] = useState({
    password: '',
  })

  const handleChange = (e) => {
    setData({[e.target.name]: e.target.value})
  }

  if(props.currentStep !== 3){
    return null
  }
  return (
    <div className= "form-group">
      <label htmlFor="password">Password</label>
      <input 
        type="password" 
        placeholder= "Enter Password"
        id="password"
        name="password"
        value={data.password}
        onChange={(e) => {handleChange(e)}}
      />
    </div>
  );
}
