import React, { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";

const MyContext = React.createContext();

function Firms() {
  const { state, setState } = useContext(MyContext);
  console.log(state)  
  useEffect(() => {
    setTimeout(() => {
      setState(state => ({
        ...state,
        name: 'sdf'
      }))
    }, 1000)

  }, [])
  return <h1>custom component {state.name}</h1>

}

function About() {
  const [obj, setObj] = useState({
    name: 'Gunjan',
    age: 24,
    firm: 'systango'
  })

  return(
    <div>
      <p>You are on About Page</p>
      <p>Take Gunjan to <Link to='/greet/Gunjan'>Greet page</Link></p>
      <MyContext.Provider value={{
          state: obj, setState: setObj
        }}>
        <p>
          <Firms />
        </p>
      </MyContext.Provider>
    </div>
  )
}

export default About;