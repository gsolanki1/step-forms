import { useState } from "react";
import '../App.css';

export const StepForm2 = (props) => {
  const [data, setData] = useState({
    username: '',
  })

  const handleChange = (e) => {
    setData({[e.target.name]: e.target.value})
  }

  if(props.currentStep !== 2){
    return null
  }
  return (
    <div className= "form-group"> 
      <label htmlFor="username">UserName</label>
      <input 
        type="text" 
        placeholder= "Enter UserName"
        id="username"
        name="username"
        value={data.username}
        onChange={(e) => {handleChange(e)}}
      />
    </div>
  );
}