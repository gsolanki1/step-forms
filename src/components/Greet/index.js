import React, { useRef, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

function Greet() {
  const params = useParams();
  const h1Ref = useRef();

  useEffect(() => {
    console.log(h1Ref)
  }, [])

  return(
    <div>
      <p ref={h1Ref}>You are on Greet Page. Hello {params.name}</p>
      <p>Go to <Link to='/'>Home PAge</Link></p>
    </div>
  )
}

export default Greet;