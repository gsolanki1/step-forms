import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { StepForm1 } from "./components/StepForm1";
import { StepForm2 } from "./components/StepForm2";
import { StepForm3 } from "./components/StepForm3";

import Home from "./components/Home/index";
import About from "./components/About/index";
import Greet from "./components/Greet/index";

import './App.css';

class MasterForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 1
    };
  }

  handleSubmit() {
    
  }

  _previous = () => {
    let currentStep = this.state.currentStep
    currentStep = (currentStep == 1) ? 1 : (currentStep - 1)
    this.setState({
      currentStep: currentStep
    })
  }

  previousButton() {
    if(this.state.currentStep !==1) {
      return(
        <button 
          className="btn btn-primary float-right" 
          type="button" 
          onClick={this._previous}>
          Previous
        </button>
      )
    }
    return null
  }

  _next = () => {
    let currentStep = this.state.currentStep
    currentStep = (currentStep == 3) ? 3 : (currentStep + 1)
    this.setState({
      currentStep: currentStep
    })
  }

  nextButton() {
    if(this.state.currentStep !==3){
      return(
        <button 
          className="btn btn-primary float-right" 
          type="button" 
          onClick={this._next}>
          Next
        </button>
      )
    }
    return null
  }
  
  render() {
    return (
      <React.Fragment>

        <Router>
          <Switch>
            <Route exact path='/' component={Home} ></Route>
            <Route exact path='/about' component={About} ></Route>
            <Route exact path='/greet/:name' component={Greet} ></Route>
          </Switch>
        </Router>
        <h1>A Wizard Form!</h1>
        <p>Step {this.state.currentStep} </p> 
          
        <form onSubmit={this.handleSubmit}>
          {/* render the form steps and pass required props in */}
          <StepForm1 
            currentStep={this.state.currentStep} 
            handleChange={this.handleChange}
            email={this.state.email}
          />
          <StepForm2 
            currentStep={this.state.currentStep} 
            handleChange={this.handleChange}
            username={this.state.username}
          />
          <StepForm3 
            currentStep={this.state.currentStep} 
            handleChange={this.handleChange}
            password={this.state.password}
          />
          {this.previousButton()}
          {this.nextButton()}
        </form>
      </React.Fragment>
    )
  }
}

export default MasterForm;
