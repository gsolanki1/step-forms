import { useState } from "react";
import '../App.css';

export const StepForm1 = (props) => {
  const [data, setData] = useState({
    email: '',
  })

  const handleChange = (e) => {
    setData({[e.target.name]: e.target.value})
  }

  if(props.currentStep !== 1){
    return null
  }
  return (
    <div className= "form-group"> 
      <label htmlFor="email">Email address</label>
      <input 
        type="text" 
        placeholder= "Enter Email Address"
        id="email"
        name="email"
        value={data.email}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
}