import React from "react";
import { Link } from "react-router-dom";

function Home() {
  return(
    <div>
      <p>You are on Home Page</p>
      <p>Go to <Link to='/about'>About PAge</Link></p>
    </div>
  )
}

export default Home;